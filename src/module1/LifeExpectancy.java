package module1;
import processing.core.*;

import java.util.HashMap;
import java.util.List;
import java.util.Map;


import de.fhpotsdam.unfolding.UnfoldingMap;
import de.fhpotsdam.unfolding.geo.Location;
import de.fhpotsdam.unfolding.providers.AbstractMapProvider;
import de.fhpotsdam.unfolding.providers.Google;
import de.fhpotsdam.unfolding.providers.MBTilesMapProvider;
import de.fhpotsdam.unfolding.utils.MapUtils;
import de.fhpotsdam.unfolding.data.*;
import de.fhpotsdam.unfolding.marker.*;

public class LifeExpectancy extends PApplet{
	
	UnfoldingMap map;
	Map<String, Float> life; 
	List<Feature> countries;
	List<Marker> countryMarkers;
	
	public void setup(){
		
		//create window for World Map
		size(800,600,OPENGL);
		map = new UnfoldingMap(this,50,50,700,500, new Google.GoogleMapProvider());
		MapUtils.createDefaultEventDispatcher(this, map);
		
//		//load data
		life = loadCSV("/Users/mfaizmzaki/LifeExpectancyWorldBankModule3.csv");
		
		//create country feature and markers
		countries = GeoJSONReader.loadData(this, "countries.geo.json");
		countryMarkers = MapUtils.createSimpleMarkers(countries);
		
		//add marker to map
		map.addMarkers(countryMarkers);
		shadeCountry();
	}
	
	public void draw(){
		map.draw();
	}
	
	private Map<String,Float> loadCSV(String filename){
		
		Map<String, Float> lifeExpMap = new HashMap<String, Float>();
		String[] rows = loadStrings(filename);
		
		for(String row : rows){
			String[] col = row.split(",");

			float value = Float.parseFloat(col[5]);
			String nameCountry = col[4];
			lifeExpMap.put(nameCountry, value);

		}
		return lifeExpMap;
	}
	
	private void shadeCountry(){
		
		for(Marker marker : countryMarkers){
			
			String countryId = marker.getId();
			
			if(life.containsKey(countryId)){
				float lifeExp = life.get(countryId);
				//translate the lifeExp float into a new range. Refer map function in PApplet
				int colorLevel = (int) map(lifeExp, 40, 90, 10, 255);
				marker.setColor(color(255-colorLevel,100,colorLevel));
			}
			else{
				marker.setColor(color(150,150,150));
			}
		}
	}
	
	private  boolean isFloat(String str) {
        try {
            Float.parseFloat(str);
            return true;
        } catch (NumberFormatException e) {
            return false;
        }
    }
	
}
