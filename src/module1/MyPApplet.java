package module1;
import processing.core.*;

public class MyPApplet extends PApplet{
	
	private String URL = "https://www.rose-hulman.edu/~cornwell/perth_pictures/small%20beach%20small.jpg";
	PImage bckgrndImg;
	
	public void setup(){
		size(300,300);
		bckgrndImg = loadImage(URL, "jpg");
	}
	
	public void draw(){
		bckgrndImg.resize(0, height);
		image(bckgrndImg, 0, 0);
		
		int[] color = changeColor(second());
		fill(color[0],color[1],color[2]);
		ellipse(width/6,height/6,width/6,height/6);
	}
	
	public int[] changeColor(float seconds){
		int[] rgb = new int[3];
		float timeDiff = Math.abs(30 - seconds);
		float ratio = timeDiff/30;
		
		rgb[0] = (int)(255*ratio);
		rgb[1] = (int)(255*ratio);
		rgb[2] = 0;
		
		return rgb;
	}
	
}
